package org.example;
import org.example.service.CashbackService;
public class Main {
    public static void main(String[] args) {
        final CashbackService service = new CashbackService();
        final int cashback = service.findOutCashback(3, 100, 300000, 42500);
        System.out.println(cashback);
    }
}
